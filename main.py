import sys
import smtplib
import crypto_util


def print_help():
    print('Usage:\n'
          '\t - python3 main.py --send from=<your gmail login> raw-password=<your password> to=<E-mail of receiver>'
          ' message=<Message>\n'
          '\t - python3 main.py --decrypt message=<Message> key=<Key>')
    exit(0)


args = sys.argv[1:]

if len(args) == 0:
    print_help()

if args[0] == '--send':
    from_ = None
    password = None

    to_ = None
    message = None

    for arg in args[1:]:
        if arg.startswith('from='):
            from_ = arg[len('from='):]
        elif arg.startswith('raw-password='):
            password = arg[len('raw-password='):]
        elif arg.startswith('to='):
            to_ = arg[len('to='):]
        elif arg.startswith('message='):
            message = arg[len('message='):]
        else:
            print(f'Unexpected token: {arg}')
            print_help()

    if from_ is None:
        print('You should specify your e-mail!')
    elif password is None:
        print('You should specify your password!')
    elif to_ is None:
        print('You should specify receiver\'s e-mail!')
    elif message is None:
        print('Please, specify message!')
    else:
        message = str(crypto_util.encrypt(message))[2:-1]

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()

        try:
            server.login(from_, password)
            server.sendmail(from_, to_, message)

            print(f'E-mail was successfully sent.\nYour encrypted message: {message}\n'
                  f'Your key: {crypto_util.KEY}\n'
                  f'\nPlease, do not share your key with others! Be secure and safe. Thanks!')
        except smtplib.SMTPAuthenticationError:
            print('Your e-mail is secured from non-Google apps. '
                  'Please, turn this setting off, if you wan to use this app')
elif args[0] == '--decrypt':
    message = None
    key = None

    for arg in args[1:]:
        if arg.startswith('key='):
            key = arg[len('key='):]
        elif arg.startswith('message='):
            message = arg[len('message='):]
        else:
            print(f'Unexpected token: {arg}')
            print_help()

    if key is None:
        print('You should specify your key for decryption!')
    elif message is None:
        print('Please, specify message!')
    else:
        print(f'Your message: "{str(crypto_util.decrypt(message, key))[2:-1]}"')
else:
    print_help()
