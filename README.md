## Примеры запуска
Прежде чем запустить приложение, пожалуйста, установите все зависимые библиотеки либо через `pip3 install <library name>`, либо указанным ниже способом:
* ```shell
  pip3 install -r requirements.txt
  ```

Теперь можно смело использовать приложение:
* Пример отправки сообщения:
  ```shell
  python3 main.py --send from=hakimov@gmail.com raw-password=MySecretPassword to=vezdekod_tester@gmail.com message="Hi! This is test message!"
  ```
  
  пример вывода:
  ```shell
  E-mail was successfully sent.
  Your encrypted message: gAAAAABghVRW-p2YjlQHtkqYfdwzOrBM9wPkDCkRfu3EmqbPdzXqtUF7BA9qa_hEYEdMQecFio7_vgdcOSRkKN5D6hQJhoWwzQ==
  Your key: WICDZFM
  ```

* Пример дешифровки сообщения:
  ```shell
  python3 main.py --decrypt message=gAAAAABghVRW-p2YjlQHtkqYfdwzOrBM9wPkDCkRfu3EmqbPdzXqtUF7BA9qa_hEYEdMQecFio7_vgdcOSRkKN5D6hQJhoWwzQ== key=WICDZFM
  ```
  
  пример вывода:
  ```shell
  Your message: "Hi! This is test message!"
  ```

Ключ дешифровки хранится в `data/key.txt`, который можно передавать по любым доступным канал, удобным для пользователей способами.
