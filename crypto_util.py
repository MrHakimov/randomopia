import base64
import random
import os

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

# upper case English letters + digits
ALPHABET = tuple([chr(i) for i in range(65, 65 + 26)] + [str(i) for i in range(10)])

should_write = False
KEY = ''.join(random.sample(ALPHABET, random.randint(5, 10)))

with open('data/key.txt', 'r') as f:
    written_key = f.read().strip()
    if written_key == '':
        should_write = True
    else:
        KEY = written_key

if should_write:
    with open('data/key.txt', 'w') as f:
        f.write(KEY)

# https://cryptography.io/en/latest/hazmat/primitives/key-derivation-functions/
SALT = os.urandom(16)
f = Fernet(base64.urlsafe_b64encode(PBKDF2HMAC(algorithm=hashes.SHA256(), length=32, salt=SALT,
                                               iterations=100000, backend=default_backend()).derive(KEY.encode())))


def encrypt(text: str):
    return f.encrypt(text.encode())


def decrypt(text: str, key: str):
    if key == KEY:
        return f.decrypt(bytes(text, 'utf-8'))
    else:
        return b'Incorrect key :('
